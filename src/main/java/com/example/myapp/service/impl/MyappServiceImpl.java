package com.example.myapp.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.myapp.model.ThirdPartyApiModel;
import com.example.myapp.service.MyappService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MyappServiceImpl implements MyappService {

	@Override
	public ThirdPartyApiModel getThirdPartyAPIJsonUsingPost(Integer userId, String title, String body) throws IOException {

		ResponseEntity<String> response = getResponse(userId, title, body);

		if (response.getStatusCode() != HttpStatus.CREATED) {
			throw new IOException();
		}

		String jsonResponse = response.getBody();

		ObjectMapper objectMapper = new ObjectMapper();
		ThirdPartyApiModel thirdPartyApiModel = objectMapper.readValue(jsonResponse.toString(),
				ThirdPartyApiModel.class);
		return thirdPartyApiModel;
	}

	/**
	 * This is using RestTemplate
	 * 
	 * @param userId
	 * @param title
	 * @param body
	 * @return
	 */
	private static ResponseEntity<String> getResponse(Integer userId, String title, String body) {

		String url = "https://jsonplaceholder.typicode.com/posts";

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		map.put("title", title);
		map.put("body", body);

		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

		ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

		return response;
	}
	
	@Override
	public ThirdPartyApiModel getThirdPartyAPIJsonUsingPut(ThirdPartyApiModel thirdPartyApiModel) throws IOException{
		
		HttpURLConnection connection = createConnection(thirdPartyApiModel);
		
		StringBuilder jsonResponse = new StringBuilder();

		int code = connection.getResponseCode();
		if (code != 200) {
			throw new IOException();
		}

		try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				jsonResponse.append(responseLine.trim());
			}
		} catch (IOException e) {
			throw new IOException(e);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		ThirdPartyApiModel thirdPartyApiModelReturned = objectMapper.readValue(jsonResponse.toString(),
				ThirdPartyApiModel.class);
		return thirdPartyApiModelReturned;
		
	}

	/**
	 * This is using HttpUrlConnection
	 * 
	 * @param userId
	 * @param title
	 * @param body
	 * @return
	 * @throws IOException
	 */
	private static HttpURLConnection createConnection(ThirdPartyApiModel thirdPartyApiModel) throws IOException {
		HttpURLConnection con = null;
		String thirdPartyAPIUrl = "https://jsonplaceholder.typicode.com/posts/1";

		try {
			URL url = new URL(thirdPartyAPIUrl);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("PUT");
			con.setRequestProperty("Accept", "*/*");
			con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			con.setDoOutput(true);
			JSONObject json = new JSONObject();
			json.put("userId", thirdPartyApiModel.getUserId());
			json.put("title", thirdPartyApiModel.getTitle());
			json.put("body", thirdPartyApiModel.getBody());
			String jsonFormatString = json.toString();

			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonFormatString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}

		} catch (IOException i) {
			throw new IOException(i);
		}
		return con;
	}

}