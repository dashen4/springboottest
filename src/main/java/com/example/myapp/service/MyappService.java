package com.example.myapp.service;

import java.io.IOException;

import com.example.myapp.model.ThirdPartyApiModel;

public interface MyappService {

	public ThirdPartyApiModel getThirdPartyAPIJsonUsingPost(Integer userId, String title, String body) throws IOException;
	
	public ThirdPartyApiModel getThirdPartyAPIJsonUsingPut(ThirdPartyApiModel thirdPartyApiModel) throws IOException;

}