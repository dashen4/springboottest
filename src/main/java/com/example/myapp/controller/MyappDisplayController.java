package com.example.myapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MyappDisplayController{
	
	@RequestMapping(value={"", "/index"}, method= RequestMethod.GET)
	public String displayIndex() {
		return "index";
	}
	
	
}