package com.example.myapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.myapp.model.MyappModel;
import com.example.myapp.model.ResultVo;
import com.example.myapp.model.ThirdPartyApiModel;
import com.example.myapp.service.MyappService;

@Controller
@RequestMapping(value = "/api")
public class MyappAPIController {

	@Autowired
	private MyappService myappService;

	@RequestMapping(value = "/getAPI", method = RequestMethod.GET)
	@ResponseBody
	public ResultVo getMyappModel(HttpServletRequest request,
			@RequestParam(name = "name", required = false) String name,
			@RequestParam(name = "hobby", required = false) String hobby) {

		MyappModel myappModel = new MyappModel();
		myappModel.setId(1);
		myappModel.setFirstString(name);
		myappModel.setSecondString(hobby);

		ResultVo resultVo = new ResultVo();
		resultVo.setSuccess(true);
		resultVo.setData(myappModel);

		return resultVo;
	}

	@RequestMapping(value = "/thirdPartyAPI", method = RequestMethod.POST)
	@ResponseBody
	public ResultVo postThirdPartyRes(HttpServletRequest request, @ModelAttribute ThirdPartyApiModel thirdPartyApimodel, @RequestParam(name="method") String method)
			throws Exception {

		ResultVo resultVo = new ResultVo();

		try {
			ThirdPartyApiModel thirdPartyApiModel = new ThirdPartyApiModel();
			if(method.equals("post")) {
				thirdPartyApiModel = myappService.getThirdPartyAPIJsonUsingPost(thirdPartyApimodel.getUserId(),
					thirdPartyApimodel.getTitle(), thirdPartyApimodel.getBody());
			}else if(method.equals("put")) {
				thirdPartyApiModel = myappService.getThirdPartyAPIJsonUsingPut(thirdPartyApimodel);
			}
			resultVo.setSuccess(true);
			resultVo.setData(thirdPartyApiModel);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

		return resultVo;
	}
}